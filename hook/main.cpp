#include "main.h"

namespace
{
    std::unordered_map<std::string_view, PROC> Originals;

    // Write "jmp <hook address>" to OpenGL function
    PROC patch_jmp(PROC oFunc, PROC nFunc, size_t count)
    {
        if (count < 5)
            return (PROC)NULL;

        DWORD oldProtect;


        BYTE *orig = (BYTE*)malloc(count);
        BYTE *jump = (BYTE*)malloc(count);
        jump[0] = 0xE9; // jmp

        memset(jump + 5, 0x90, count - 5);
        *(DWORD*)(jump + 1) = (DWORD)nFunc - (DWORD)oFunc - 5;

        memcpy(orig, (VOID*)oFunc, count);
        memcpy((VOID*)oFunc, jump, count);
        free(jump);
        VirtualProtect((VOID*)orig, count, PAGE_EXECUTE_READWRITE, &oldProtect);
        return (PROC)orig;
    }
}

void (*SDL_RenderPresent_Orig)(SDL_Renderer*);


SDL_Renderer* (*SDL_CreateRenderer_Orig)(SDL_Window*, int, Uint32);
int (*SDL_PollEvent_Orig)(SDL_Event*);
SOCKET pyserv_sock;

int connect_to_pyserv()
{
    WSADATA wsa;


	SDL_Log("Initialising Winsock...\n");

	if (WSAStartup(MAKEWORD(2,2), &wsa) != 0)
	{
		SDL_Log("Failed. Error Code : %d\n", WSAGetLastError());
		return 1;
	}

	SDL_Log("Initialised.\n");

	return 0;
}

bool saveScreenshot(const char *file, SDL_Renderer *renderer)
{
    int w, h;
    SDL_Surface *_surface = NULL;

    SDL_GetRendererOutputSize(renderer, &w, &h);

    _surface = SDL_CreateRGBSurfaceWithFormat( 0, w, h, 32, SDL_PIXELFORMAT_RGBA32);
    SDL_Log("Width: %i\nHeight: %i\n", w, h);

    if ( _surface == NULL )
    {
        SDL_Log("Cannot create SDL_Surface: %s\n", SDL_GetError());
        return false;
    }

    SDL_LockSurface(_surface);

    if (SDL_RenderReadPixels(renderer, NULL, _surface->format->format, _surface->pixels,
                             _surface->pitch) != 0 )
    {
        SDL_Log("Cannot read data from SDL_Renderer: %s\n", SDL_GetError());
        SDL_UnlockSurface(_surface);
        SDL_FreeSurface(_surface);
        return false;
    }

    if ((pyserv_sock = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		SDL_Log("Could not create socket : %d\n", WSAGetLastError());
		return 1;
	}

	SDL_Log("Socket created.\n");

    struct sockaddr_in server;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons(34657);

    while (connect(pyserv_sock, (struct sockaddr*)&server, sizeof(server)) < 0)
	{
		SDL_Log("connect error %d\n", WSAGetLastError());
	}

	SDL_Log("Connected\n");

	int sended = 0;
	int max = w * h * 4;
	char _b;

	while (true)
    {
       if ((send(pyserv_sock, (const char*)_surface->pixels + sended,
                          (((max - sended) < 48000) ? (max - sended) : 48000), 0) < 0))
        {
            SDL_Log("Send failed\n");
            return 1;
        }

        sended += 48000;
        SDL_Log("Data Send: %i\n", sended);

        if (sended >= max)
            break;
    }

    SDL_UnlockSurface(_surface);

    closesocket(pyserv_sock);

    SDL_FreeSurface(_surface);
    return true;
}

void SDL_RenderPresent_Hook(SDL_Renderer* renderer)
{
    static int a = 0;
    ++a;
    if (a >= 25)
    {
        saveScreenshot("OPA34.bmp", renderer);
        a = 0;
    }

    SDL_RenderPresent_Orig(renderer);
}

SDL_Renderer* SDL_CreateRenderer_Hook(SDL_Window* window,
                                 int         index,
                                 Uint32      flags)
{
    SDL_Log("%i\n", window);
    return SDL_CreateRenderer_Orig(window, index, flags);
}

int SDL_PollEvent_Hook(SDL_Event *event)
{
    SDL_Log("%i\n", event);
    return SDL_PollEvent_Orig(event);
}

extern "C" DLL_EXPORT BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
    {
        // attach to process
        // return FALSE to fail DLL load
        case DLL_PROCESS_ATTACH:
            {
                AllocConsole();
                connect_to_pyserv();
                SDL_Log("HELLO\n");
                HMODULE sdl2_dll = GetModuleHandle("SDL2.dll");
                SDL_Log("%x\n", sdl2_dll);

                SDL_RenderPresent_Orig =
                (void(*)(SDL_Renderer*))GetProcAddress(sdl2_dll, "SDL_RenderPresent");
                SDL_Log("%x\n", SDL_RenderPresent_Orig);

                MODULEINFO sdl2_info;
                DWORD oldProtect;
                GetModuleInformation(GetCurrentProcess(), sdl2_dll, &sdl2_info, sizeof(sdl2_info));
                SDL_Log("%i\n", sdl2_info.SizeOfImage);

                if (VirtualProtect((VOID*)sdl2_dll, sdl2_info.SizeOfImage, PAGE_EXECUTE_READWRITE, &oldProtect) != 0)
                {
                    SDL_RenderPresent_Orig = (void(*)(SDL_Renderer*))patch_jmp((PROC)SDL_RenderPresent_Orig, (PROC)SDL_RenderPresent_Hook, 16);
                    SDL_Log("%x\n", (void*)SDL_RenderPresent_Orig);
                } else
                    SDL_Log("VirtualProtect error!\n");

                while (true);

                break;
            }

        case DLL_PROCESS_DETACH:
            // detach from process
            break;

        case DLL_THREAD_ATTACH:
            // attach to thread
            break;

        case DLL_THREAD_DETACH:
            // detach from thread
            break;
    }
    return TRUE; // succesful
}
