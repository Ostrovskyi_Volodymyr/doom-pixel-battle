#ifndef __MAIN_H__
#define __MAIN_H__

#include <winsock.h>
#include <winsock2.h>
#include <windows.h>
#include <psapi.h>
#include <imagehlp.h>
#include <dbghelp.h>
#include <cstdio>
#include <unordered_map>
#include <SDL2/SDL.h>

#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif


#ifdef __cplusplus
extern "C"
{
#endif

//void DLL_EXPORT SomeFunction(const LPCSTR sometext);

#ifdef __cplusplus
}
#endif

#endif // __MAIN_H__
