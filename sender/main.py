import vk_api
import requests
import websockets
import execjs
import asyncio
import datetime
import math
import socket
from PIL import Image

MAX_WIDTH = 1590
MAX_HEIGHT = 400
MAX_COLOR_ID = 25
MIN_COLOR_ID = 0
SIZE = MAX_WIDTH * MAX_HEIGHT

def pack(color_id, flag, x, y):
    return ((color_id + flag * MAX_COLOR_ID) * SIZE + x + y * MAX_WIDTH).to_bytes(4, 'little')

async def draw_image(ws, img, oimg, x, y, w, h):
    print('draw_image')
    for i in range(y, h):
        for j in range(x, w):

            if len(oimg) and oimg[i * w + j] == img[i * w + j]:
                continue

            if img[i * w + j] < 128:
                await ws.send(pack(4, 0, j + 1, i + 1))
            else:
                await ws.send(pack(0, 0, j + 1, i + 1))
    await ws.send(f'ping')

async def get_websocket():
    start = requests.Session()
    start.headers.update({
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Cookie': '__cfduid=ddff5422864cc19e6e99efc82682a69e11568824851',
        'DNT': '1',
        'Host': '<>',
        'Referer': '<>',#'Referer': '<>',
        'If-Modified-Since': 'Mon, 14 Oct 2019 21:56:17 GMT',
        'If-None-Match': '\"8d750f15728a75d\"',
        'Upgrade-Insecure-Requests': '1',
        'X-vk-sign': '',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
    })
    con_data = start.get('<>').json()
    print(start.headers)
    print(con_data)
    m = datetime.datetime.utcnow().minute
    h = datetime.datetime.utcnow().hour
    secure = math.floor(math.sin(m + 28) * 100 + h * 15) % 155
    wurl = con_data['response']['url']
    sign = 9 #con_data['response']['sign'] + secure
    hackc = '''
    var global = undefined;
    '''
    code =  execjs.compile(hackc).eval(con_data['response']['code'])
    print(sign)
    print(code)
    print(wurl)

    ws = await websockets.connect(f'<>:9123{wurl}?s={sign}&c={code}')

    await ws.send('R1')
    print(await ws.recv())
    print(await ws.send(f'ping'))
    print(await ws.recv())
    print(await ws.send(f'ping'))

    return ws

async def ws_send(ws, m):
    while True:
        try:
            await ws.send(m)
            break
        except:
            ws.close()
            ws = await get_websocket()
    return ws

async def ws_recv(ws):
    while True:
        try:
            return ws, await ws.recv()
            break
        except:
            ws.close()
            ws = await get_websocket()

async def main():

    ws1 = await get_websocket()

    host = '127.0.0.1'
    port = 34657
    image_width = 921
    image_height = 691

    serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serv_sock.bind((host, port))
    serv_sock.listen()

    arr = []
    old_arr = []

    while True:

        client, addr = serv_sock.accept()
        received = 0

        with client:
            print('Connected by', addr)
            try:
                data = client.recv(48000)
                received += 48000
                while True:
                    td = client.recv(48000)
                    received += 48000
                    if not td:
                        break
                    data += td
            except:
                pass
            print('Received!')
            print(len(data))
            im = Image.frombuffer('RGBA', (image_width, image_height), data)
            im.thumbnail((153, 115))
            im = im.rotate(180)
            im = im.transpose(Image.FLIP_LEFT_RIGHT)
            gray = im.convert('1')
            arr = list(gray.getdata())
            art_x, art_y = 1, 1
            gw, gh = gray.size
            print(f"Size: {gw} {gh}")
            print(len(arr))

            for i in range(0, gh):
                for j in range(0, gw):

                    if len(old_arr) and old_arr[i * gw + j] == arr[i * gw + j]:
                        continue

                    if arr[i * gw + j] < 128:
                        ws1 = await ws_send(ws1, pack(4, 0, j + art_x, i + art_y))
                    else:
                        ws1 = await ws_send(ws1, pack(0, 0, j + art_x, i + art_y))
            ws1 = await ws_send(ws1,'ping')
            old_arr = arr
            client.close()

if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
